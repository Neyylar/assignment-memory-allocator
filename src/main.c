#include "mem.h"




int main(){
    heap_init(5000);


    printf("Test1: allocation\n");
    void *a = _malloc(20);
    void *b = _malloc(200);
    void *c = _malloc(200);
    void *d = _malloc(50);
    debug_heap(stdout, HEAP_START);


    printf("Test2: freeing\n");
    _free(a);
    _free(c);
    debug_heap(stdout, HEAP_START);
    a = _malloc(10);
    debug_heap(stdout, HEAP_START);


    printf("Test3: merging\n");
    _free(b);
    _free(d);
    debug_heap(stdout, HEAP_START);
}
